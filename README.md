
Setup
$ git clone https://OlyaOlya@bitbucket.org/OlyaOlya/courses.git
$ cd courses
$ python3 -m venv myvenv
$ . myvenv/bin/activate

(myenv)$ pip install -r requirements.txt
(myenv)$ python manage.py runserver

Example

* Додавання курсу в каталог
http://127.0.0.1:8000/course/add_course/?date_start=01/08/2021&quantity_lectures=200&name=python&date_end=01/06/2022
[{"result": "success", "data": "Courses added"}]

* Відображення списку курсів
http://127.0.0.1:8000/course/all_courses/
[{"id": 1, "name": "C++", "date_start": "2017-01-01T00:00:00+00:00", "date_end": "2018-01-01T00:00:00+00:00", "quantity_lectures": 150}, {"id": 2, "name": "python", "date_start": "2021-08-01T00:00:00+00:00", "date_end": "2022-06-01T00:00:00+00:00", "quantity_lectures": 200}, {"id": 3, "name": "php", "date_start": "2024-08-01T00:00:00+00:00", "date_end": "2025-06-01T00:00:00+00:00", "quantity_lectures": 200}]

* Відображення деталей курсу по id (детальна сторінка курсу повинна відображати повну інформацію про курс)
http://127.0.0.1:8000/course/detail_course/2/
[{"id": 2, "name": "python", "date_start": "2021-08-01T00:00:00+00:00", "date_end": "2022-06-01T00:00:00+00:00", "quantity_lectures": 200}]

* Зміна атрибутів курсу
в будь-якій комбінації параметрів, приклад деяких варіантів
http://127.0.0.1:8000/course/edit_course/2/?quantity_lectures=100&name=python django&date_end=01/09/2022&date_start=01/09/2021
[{"result": "success", "data": "Courses edited"}]

http://127.0.0.1:8000/course/edit_course/2/?name=python django1&date_end=01/09/2022&date_start=01/09/2021
[{"result": "success", "data": "Courses edited"}]

http://127.0.0.1:8000/course/edit_course/2/?name=python django
[{"result": "success", "data": "Courses edited"}]

http://127.0.0.1:8000/course/edit_course/2/?quantity_lectures=100&name=python django&date_end=01/09/2022
[{"result": "success", "data": "Courses edited"}]

http://127.0.0.1:8000/course/edit_course/2/date_start=01/09/2021
[{"result": "success", "data": "Courses edited"}]

* Видалення курсу
http://127.0.0.1:8000/course/delete_course/5/
[{"result": "success", "data": "Courses deleted"}]

* Пошук курсу за назвою і фільтр по датах
в будь-якій комбінації параметрів, приклад деяких варіантів
http://127.0.0.1:8000/course/find_course/?name=python
[{"id": 2, "name": "python django", "date_start": "2021-09-01T00:00:00+00:00", "date_end": "2022-09-01T00:00:00+00:00", "quantity_lectures": 100}]

http://127.0.0.1:8000/course/find_course/?date_end1=01/09/2019&name=python&date_start2=01/08/2023&date_end2=01/09/2022&date_start1=01/09/2018
[{"id": 2, "name": "python django", "date_start": "2021-09-01T00:00:00+00:00", "date_end": "2022-09-01T00:00:00+00:00", "quantity_lectures": 100}]

http://127.0.0.1:8000/course/find_course/?date_end1=01/09/2020&name=python&date_start2=01/08/2023&date_end2=01/09/2022&date_start1=01/10/2021
[]

http://127.0.0.1:8000/course/find_course/?date_start2=01/08/2024&date_start1=01/10/2021
[{"id": 8, "name": "php", "date_start": "2024-08-01T00:00:00+00:00", "date_end": "2025-06-01T00:00:00+00:00", "quantity_lectures": 200}]

http://127.0.0.1:8000/course/find_course/?date_end2=01/06/2025&date_start1=01/10/2020
[{"id": 6, "name": "python django", "date_start": "2021-09-01T00:00:00+00:00", "date_end": "2022-09-01T00:00:00+00:00", "quantity_lectures": 100}, {"id": 8, "name": "php", "date_start": "2024-08-01T00:00:00+00:00", "date_end": "2025-06-01T00:00:00+00:00", "quantity_lectures": 200}]