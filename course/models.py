from django.db import models


class Course(models.Model):
    name = models.CharField(max_length=500)
    date_start = models.DateTimeField(blank=True, null=True)
    date_end = models.DateTimeField(blank=True, null=True)
    quantity_lectures = models.IntegerField()
