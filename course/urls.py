from django.conf.urls import url
from course import views

urlpatterns = [
    url(r'^all_courses/$', views.all_courses, name='all_courses'),
    url(r'^add_course/$', views.add_course, name='add_course'),
    url(r'^delete_course/(?P<course_id>[0-9]+)/$', views.delete_course, name='delete_course'),
    url(r'^detail_course/(?P<course_id>[0-9]+)/$', views.detail_course, name='detail_course'),
    url(r'^edit_course/(?P<course_id>[0-9]+)/$', views.edit_course, name='edit_course'),
    url(r'^find_course/$', views.find_course, name='find_course'),
]
