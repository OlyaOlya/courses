from django.shortcuts import render
from course.models import Course
from django.http import HttpResponse
import json
import datetime
from django.shortcuts import get_object_or_404
from django.db.models import Q

DATE_NOW = datetime.datetime.now().date()
START = datetime.datetime.strptime('01/01/1900', "%d/%m/%Y")
END = datetime.datetime.strptime('01/01/2100', "%d/%m/%Y")


def all_courses(request):
    courses = Course.objects.all()
    data = [{'id': course.id,
             'name': course.name,
             'date_start': course.date_start,
             'date_end': course.date_end,
             'quantity_lectures': course.quantity_lectures,
             } for course in courses]

    def default(o):
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()

    return HttpResponse(json.dumps(data, default=default))


def add_course(request):
    name = request.GET.get('name', None)
    date_start = request.GET.get('date_start', None)
    date_end = request.GET.get('date_end', None)
    quantity_lectures = request.GET.get('quantity_lectures', None)

    try:
        date_start = datetime.datetime.strptime(date_start, "%d/%m/%Y")
        date_end = datetime.datetime.strptime(date_end, "%d/%m/%Y")
        insert_course = Course(name=name,
                               date_start=date_start,
                               date_end=date_end,
                               quantity_lectures=quantity_lectures,
                               )
        insert_course.save()
        data = [{'result': 'success',
                'data': 'Courses added'}]

    except Exception as ex:
        print(ex)
        data = [{'result': 'error',
                 'data': ex}]

    return HttpResponse(json.dumps(data))


def delete_course(request, course_id):
    obj = get_object_or_404(Course, pk=course_id)
    Course.objects.filter(pk=obj.pk).delete()
    data = [{'result': 'success',
             'data': 'Courses deleted'}]
    return HttpResponse(json.dumps(data))


def detail_course(request, course_id):
    course = Course.objects.get(pk=course_id)
    data = [{'id': course.id,
             'name': course.name,
             'date_start': course.date_start,
             'date_end': course.date_end,
             'quantity_lectures': course.quantity_lectures,
             }]

    def default(o):
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()

    return HttpResponse(json.dumps(data, default=default))


def edit_course(request, course_id):
    course = Course.objects.get(pk=course_id)
    name = request.GET.get('name', None)
    date_start = request.GET.get('date_start', None)
    date_end = request.GET.get('date_end', None)
    quantity_lectures = request.GET.get('quantity_lectures', None)
    try:
        if name == '' or name is None:
            name = course.name
        if date_start == '' or date_start is None:
            date_start = course.date_start
        else:
            date_start = datetime.datetime.strptime(date_start, "%d/%m/%Y")

        if date_end == '' or date_end is None:
            date_end = course.date_end
        else:
            date_end = datetime.datetime.strptime(date_end, "%d/%m/%Y")

        if quantity_lectures == '' or quantity_lectures is None:
            quantity_lectures = course.quantity_lectures

        Course.objects.filter(pk=course_id).update(name=name,
                                                   date_start=date_start,
                                                   date_end=date_end,
                                                   quantity_lectures=quantity_lectures,)

        data = [{'result': 'success',
                 'data': 'Courses edited'}]
    except Exception as ex:
        print(ex)
        data = [{'result': 'error',
                 'data': ex}]

    def default(o):
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()

    return HttpResponse(json.dumps(data, default=default))


def find_course(request):
    name = request.GET.get('name', None)
    date_start_1 = request.GET.get('date_start1', None)
    date_start_2 = request.GET.get('date_start2', None)
    date_end_1 = request.GET.get('date_end1', None)
    date_end_2 = request.GET.get('date_end2', None)

    result = []

    if (name != '' and name is not None) and (date_start_1 == '' or date_start_1 is None) and \
            (date_end_2 == '' or date_end_2 is None) and (date_start_2 == '' or date_start_2 is None) and \
            (date_end_1 == '' or date_end_1 is None):
        result = Course.objects.filter(Q(name__icontains=name))
        print(result)
    elif name == '' or name is None:
        if (date_start_1 != '' or date_start_1 is not None) or \
            (date_end_1 != '' or date_end_1 is not None) or \
                (date_start_2 != '' or date_start_2 is not None) or \
                (date_end_2 != '' or date_end_2 is not None):

            if date_start_1 == '' or date_start_1 is None:
                date_start_1 = DATE_NOW
            else:
                date_start_1 = datetime.datetime.strptime(date_start_1, "%d/%m/%Y")

            if date_start_2 == '' or date_start_2 is None:
                date_start_2 = END
            else:
                date_start_2 = datetime.datetime.strptime(date_start_2, "%d/%m/%Y")

            if date_end_2 == '' or date_end_2 is None:
                date_end_2 = END
            else:
                date_end_2 = datetime.datetime.strptime(date_end_2, "%d/%m/%Y")

            if date_end_1 == '' or date_end_1 is None:
                date_end_1 = DATE_NOW
            else:
                date_end_1 = datetime.datetime.strptime(date_end_1, "%d/%m/%Y")

            result = Course.objects.filter(Q(date_start__range=(date_start_1, date_start_2)) &
                                           Q(date_start__range=(date_end_1, date_end_2)))
    else:
        date_start_1 = datetime.datetime.strptime(date_start_1, "%d/%m/%Y")
        date_end_1 = datetime.datetime.strptime(date_end_1, "%d/%m/%Y")
        date_start_2 = datetime.datetime.strptime(date_start_2, "%d/%m/%Y")
        date_end_2 = datetime.datetime.strptime(date_end_2, "%d/%m/%Y")
        result = Course.objects.filter(Q(name__icontains=name) &
                                       Q(date_start__range=(date_start_1, date_start_2)) &
                                       Q(date_start__range=(date_end_1, date_end_2)))
        print(result)

    def default(o):
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()

    data = [{'id': course.id,
             'name': course.name,
             'date_start': course.date_start,
             'date_end': course.date_end,
             'quantity_lectures': course.quantity_lectures,
             } for course in result]

    return HttpResponse(json.dumps(data, default=default))
